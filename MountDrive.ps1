###################################################################################
## Set-ExecutionPolicy bypass
##
## PowerShell script used to mount :
## - homedir : \\ad.imta.fr\homes\login
## - sanssauvegarde personal storage : \\sanssauvegarde.svc.enst-bretagne.fr\login
## - sanssauvegarde mee : \\sanssauvegarde.svc.enst-bretagne.fr\mee
##
## Should be used in SALSA VLAN
##
##
###################################################################################

echo "###################################################################################"
echo "###################################################################################"
echo "###################################################################################"
echo ""
echo "This script is used to mount :"
echo " - homedir : \\ad.imta.fr\homes\login"
echo " - sanssauvegarde personal storage : \\sanssauvegarde.svc.enst-bretagne.fr\login"
echo " - sanssauvegarde MEE : \\sanssauvegarde.svc.enst-bretagne.fr\mee"
echo ""
echo "On the first available drive letters"
echo ""
echo "Should be used in SALSA network only"
echo ""
echo "###################################################################################"
echo "###################################################################################"
echo "###################################################################################"
echo ""

## get user AD login and password via a dedicated graphical window
$Credential = Get-Credential
$NetworkCredential = $Credential.GetNetworkCredential()

if($NetworkCredential.Domain){
    $User = "$($NetworkCredential.Domain)\$($NetworkCredential.UserName)"
}
else{
    $User = "ad\"+"$($NetworkCredential.UserName)"
}
$Password=$NetworkCredential.Password

###################################################################################
## remove register base entry concerning previous mount of concerned drives

$KeyHomedir = "hkcu:\Software\Microsoft\Windows\CurrentVersion\Explorer\MountPoints2\##ad.imta.fr#homes#"+$($NetworkCredential.UserName)
$KeySSP     = "hkcu:\Software\Microsoft\Windows\CurrentVersion\Explorer\MountPoints2\##sanssauvegarde.svc.enst-bretagne.fr#"+$($NetworkCredential.UserName)

echo $KeyHomedir
echo $KeySSP

Remove-Item -Path $KeyHomedir
Remove-Item -Path $KeySSP
Remove-Item -Path 'hkcu:\Software\Microsoft\Windows\CurrentVersion\Explorer\MountPoints2\##sanssauvegarde.svc.enst-bretagne.fr#mee'


###################################################################################
## remove all network drives

echo "Supprime toutes les connexions de lecteurs réseaux existantes dans Ordinateur"

net use x: /del /yes
net use y: /del /yes
net use z: /del /yes


###################################################################################
## homedir info and mount

$NetworkHomedir = "\\ad.imta.fr\homes\"+$($NetworkCredential.UserName)
$WSHNetworkHomedir = New-Object -Com WScript.Network
try{
    $WSHNetworkHomedir.MapNetworkDrive("z:",$NetworkHomedir,$true,$User,$Password)
}
catch{
    Write-Error "Une erreur s'est produite lors du montage du homedir : $($_.Exception.MEssage)"
}


###################################################################################
## sanssauvegarde personal storage info and mount

$NetworkSanssauvegardePerso = "\\sanssauvegarde.svc.enst-bretagne.fr\"+$($NetworkCredential.UserName)
$WSHNetworkSanssauvegardePerso = New-Object -Com WScript.Network
try{
    $WSHNetworkSanssauvegardePerso.MapNetworkDrive("y:",$NetworkSanssauvegardePerso,$true,$User,$Password)
}
catch{
    Write-Error "Une erreur s'est produite lors du montage de sanssauvegarde personel : $($_.Exception.MEssage)"
}


###################################################################################
## sanssauvegarde mee info  and mount

$NetworkSanssauvegardeMee = "\\sanssauvegarde.svc.enst-bretagne.fr\mee"
$WSHNetworkSanssauvegardeMee = New-Object -Com WScript.Network
try{
    $WSHNetworkSanssauvegardeMee.MapNetworkDrive("x:",$NetworkSanssauvegardeMee,$true,$User,$Password)
}
catch{
    Write-Error "Une erreur s'est produite lors du montage de sanssauvegarde mee : $($_.Exception.MEssage)"
}

sleep 20
