#+author: Jean-Noël Bazin
#+LANGUAGE: en

* Overview
Collection of scripts used to simplify the use of windows desktop computer on the SALSA network.
- One set of scripts (drive.bat and MountDrive.ps1)  can be used to mount all the network drives of a user (sanssauvegarde/mee, sanssauvegarde/user_login, campus/user_login...)
- The other set of scripts (imp.bat and MountPrinter.ps1) can be used to connect all the printers of the MEE dept.

* Architecture
For each set of script has the same behavior, the .bat file calls its corresponding .ps1 powershell script with the appropriate security policy without the need to change the global default security policy.

* Installation
- The .ps1 scripts MUST be on C:
- The .bat scripts can be placed anywhere.

* Use
Just double click on the .bat file, then enter your login and password. It usually takes 10~20 second max for the drive.bat script, and up to 1 minute for the imp.bat, due to drivers installation.
