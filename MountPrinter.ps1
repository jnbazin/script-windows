###################################################################################
## Set-ExecutionPolicy bypass
##
## PowerShell script used to connect printers of Dept ELEC
##
## Should be used in SALSA VLAN
##
###################################################################################

function Get-NextFreeDrive {
  68..90 | ForEach-Object { "$([char]$_):" } |
  Where-Object { 'h:', 'k:', 'z:' -notcontains $_  } |
  Where-Object {
    (new-object System.IO.DriveInfo $_).DriveType -eq 'noRootdirectory'
  }
}

## get user campus login and password via a dedicated graphical window
$Credential = Get-Credential
$NetworkCredential = $Credential.GetNetworkCredential()

if($NetworkCredential.Domain){
    $User = "$($NetworkCredential.Domain)\$($NetworkCredential.UserName)"
}
else{
    $User = "campus\"+"$($NetworkCredential.UserName)"
}
$Password=$NetworkCredential.Password

sleep 2

Remove-Item -Path 'hkcu:\Software\Microsoft\Windows\CurrentVersion\Explorer\MountPoints2\##vss-winprint-02.priv.enst-bretagne.fr#print$'

sleep 5
net use p: /del /yes
sleep 5
##Remove Printers
Get-WMIObject Win32_Printer | where{$_.Network -eq 'true'} | foreach{$_.delete()}

sleep 5

############
## Montage des imprimantes

## mount vss-winprint-02.priv.enst-bretagne.fr\print$
$Networkvsswinprint = "\\vss-winprint-02.priv.enst-bretagne.fr\print$"
$WSHNetworkvsswinprint = New-Object -Com WScript.Network
$lettervsswinprint=(Get-NextFreeDrive)[0]
try{
    # $WSHNetworkvsswinprint.MapNetworkDrive($lettervsswinprint,$Networkvsswinprint,$true,$User,$Password)
    $WSHNetworkvsswinprint.MapNetworkDrive("p:",$Networkvsswinprint,$true,$User,$Password)
}
catch{
    Write-Error "Une erreur s'est produite lors du montage de vss-winprint-02.priv.enst-bretagne.fr print : $($_.Exception.MEssage)"
}

sleep 25

# echo "copieur-k2"
# net use lpt2: \\vss-winprint-02.priv.enst-bretagne.fr\copieur-k2 /user:$User $Password /persistent:yes
rundll32 printui.dll,PrintUIEntry /in /n \\vss-winprint-02.priv.enst-bretagne.fr\copieur-k2
sleep 5

# echo "copieur-k1"
# net use lpt2: \\vss-winprint-02.priv.enst-bretagne.fr\copieur-k1 /user:$User $Password /persistent:yes
rundll32 printui.dll,PrintUIEntry /in /n \\vss-winprint-02.priv.enst-bretagne.fr\copieur-k1
sleep 5

# echo "IMP-ELEC-002"
# net use lpt2: \\vss-winprint-02.priv.enst-bretagne.fr\IMP-ELEC-002 /user:$User $Password /persistent:yes
# sleep 4
rundll32 printui.dll,PrintUIEntry /in /n \\vss-winprint-02.priv.enst-bretagne.fr\IMP-ELEC-002
sleep 5

# echo "IMP-ELEC-003"
# net use lpt2: \\vss-winprint-02.priv.enst-bretagne.fr\IMP-ELEC-003 /user:$User $Password /persistent:yes
# sleep 4
rundll32 printui.dll,PrintUIEntry /in /n \\vss-winprint-02.priv.enst-bretagne.fr\IMP-ELEC-003








# $printer = Get-WmiObject -Query "Select * From Win32_Printer Where Name='copieur-k2'"
# (New-Object -ComObject WScript.Network).SetDefaultPrinter($printer)
# (New-Object -ComObject WScript.Network).SetDefaultPrinter('copieur-k2')
